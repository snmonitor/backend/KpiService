export function isNullEmptyOrWhiteSpace(str?: string): boolean {
    return str===undefined || str === null || str?.match(/^\s*$/) !== null || str === "null";
}

export function ValidateEmail(email: string): boolean {

    const validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;

    return !!email.match(validRegex);

}

export function isIterable (obj:any) {
    return !!obj.forEach;
}

