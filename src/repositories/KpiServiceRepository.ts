import moment from 'moment';
import { isNullEmptyOrWhiteSpace } from '../helper';
import { KpiHistoryModel } from '../schemas'


export class KpiHistoryRepository {

    private kpiHistoryModel = KpiHistoryModel;

    async getKpiHistoryById(id: string): Promise<any> {
        return this.kpiHistoryModel.findById(id)
    }

    async KpiHistoryExist(id: string) {
        return this.kpiHistoryModel.findById(id).select('_id');
    }

    //get with pagination
    async getKpiHistorys(skip = 0, limit = 0): Promise<any> {
        const KpiHistorys = await this.kpiHistoryModel.find().skip(skip).limit(limit);
        const tradesCollectionCount = await this.kpiHistoryModel.count()
        const totalPages = limit === 0 ? 1 : Math.ceil(tradesCollectionCount / limit)
        const currentPage = skip === 0 ? 1 : Math.ceil(((skip + 1) / limit))
        return {
            pagination: {
                total: tradesCollectionCount,
                currentpage: currentPage === 0 ? 1 : currentPage,
                pages: totalPages,
            }, KpiHistorys
        }

    }

    async createKpiHistory(data: { [key: string]: any }): Promise<object> {
        const KpiHistory = new this.kpiHistoryModel(data);
        await KpiHistory.save();
        return KpiHistory
    }

    async patchKpiHistory(data: { [key: string]: any }): Promise<object> {
        return this.kpiHistoryModel.findOneAndUpdate({ _id: data._id }, data) as object;
    }

    async getKpiHistoryByDriver(driverId: string) {
        return this.kpiHistoryModel.find({ driverId: driverId }) as any;
    }

    async getKpiHistoryByDate(from: string, to: string, driverId?: string) {

        const filter = isNullEmptyOrWhiteSpace(driverId) ? {
            createdAt: {
                $gte: moment(from).toISOString(),
                $lt: moment(to).toISOString()
            },
        } : {
            createdAt: {
                $gte: moment(from).toISOString(),
                $lt: moment(to).toISOString()
            },
            driverId: driverId
        }

        return this.kpiHistoryModel.find(filter) as any;
    }

    async deleteKpiHistory(id: string): Promise<object> {
        return this.kpiHistoryModel.findOneAndDelete({ _id: id }) as object;
    }

    async deleteKpiHistoryByDriverId(driverId: string) {
        return this.kpiHistoryModel.deleteMany({ driverId: driverId }) as any;
    }


}