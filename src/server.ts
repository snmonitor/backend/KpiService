//process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'
import App from './app';
import {
     KpiHistoryController, EmailController
} from "./controllers";


const port = 1113;

const server = new App([EmailController, KpiHistoryController]);

server.start(port, `server available on http://localhost:${port}/api`);

