import {IValue} from "./IValue";

export interface IAction {
    description: string,
    valueType: string,
    values: [IValue]
}





