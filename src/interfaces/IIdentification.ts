
export interface IIdentification {
    name: string
    identifier: string
    expectedValue: string
    resourceSha1: string
    operator: string
    templateUuid: string,
}





