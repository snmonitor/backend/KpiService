import {IProduct} from "../interfaces"

export interface IConstructor {
    name: string;
    url: string;
    logo: string;
    products: [IProduct]
}





