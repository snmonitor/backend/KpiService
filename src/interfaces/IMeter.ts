export interface IMeter {
    unit: string,
    minValue: number,
    maxValue: number,
    precision: number,
    mathExpression: string
}





