import {ObjectId} from 'mongoose';

export interface IValue {
    value: string;
    templateUuid: ObjectId;
    name: string;
}





