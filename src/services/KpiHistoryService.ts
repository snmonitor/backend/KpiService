import { KpiHistoryRepository } from "../repositories";
import { isValidObjectId } from "mongoose";
import {
    BadRequest400Response,
    ConnectedWithDevice200Response,
    Created201Response,
    Deleted200Response,
    FieldMissing400Response,
    Found200Response,
    HttpException,
    IPHostNotFound400Response,
    NoDataReturnedFromOID400Response,
    NotFound404Response,
    NotUUID404Response,
    SetActionSuccess200Response,
    Updated200Response,
    WrongDevice400Response
} from "../responses";
import config from 'config'
import { isNullEmptyOrWhiteSpace } from '../helper';
import Instance from '../instance';
import { Server, Socket } from 'socket.io';
import axios from 'axios';
import moment from "moment";
var snmp = require("net-snmp");


export class KpiHistoryService {

    private promises: HttpException[] = []
    private kpiHistoryRepository = new KpiHistoryRepository();
    private token = config.get('API_TOKEN');
    private gatewayHost = config.get('GATEWAY_HOST')
    async getKpiHistoryById(id: string): Promise<HttpException> {
        if (isValidObjectId(id)) {
            const KpiHistory = await this.kpiHistoryRepository.getKpiHistoryById(id);
            if (KpiHistory !== null) {
                return new Found200Response('KpiHistory', 1, KpiHistory);
            } else {
                return new NotFound404Response(id, 'KpiHistory');
            }
        } else
            return new NotUUID404Response();
    }

    async getKpiHistoryByDriver(id: string): Promise<HttpException> {
        if (isValidObjectId(id)) {
            const KpiHistory = await this.kpiHistoryRepository.getKpiHistoryByDriver(id);
            if (KpiHistory !== null) {
                return new Found200Response('KpiHistory', KpiHistory?.length, KpiHistory);
            } else {
                return new NotFound404Response(id, 'KpiHistory');
            }
        } else
            return new NotUUID404Response("driverId");
    }

    async getKpiHistoryByDate(from: string, to: string, driverId: string): Promise<HttpException> {
        const field = this.checkDateRequired({ from, to })
        if (field == "true") {
            const KpiHistory = await this.kpiHistoryRepository
                .getKpiHistoryByDate(from, to,
                    (isValidObjectId(driverId) ? driverId : undefined));
            if (KpiHistory !== null) {
                return new Found200Response('KpiHistory', KpiHistory?.length, KpiHistory);
            } else {
                return new NotFound404Response(driverId, 'KpiHistory');
            }
        }
        else
            return new FieldMissing400Response(field)
    }

    async monitor(io: Server) {
        try {
            //Listen for a client connection 
            io.on("connection", (socket: Socket) => {
                Instance.get().logger.verbose(`client ${socket.id} connected`);
                io.emit("users", io.sockets.server.engine.clientsCount)
                socket.on('disconnect', () => {
                    io.emit("users", io.sockets.server.engine.clientsCount)
                    Instance.get().logger.silly(`client ${socket.id} disconnected`);
                });
            })


            const maxSensor: number = await config.get("SENSOR_PER_SCAN")
            const delay: number = await config.get("SCAN_INTERVAL")
            this.monitoIteration(io, maxSensor)

            //every delay
            setInterval(async () => this.monitoIteration(io, maxSensor), delay)
        }

        catch (e: any) {
            Instance.get().logger.error(e.message)
        }

    }

    async monitoIteration(io: Server, maxSensor: number) {
        //get all driver from DB
        // const token = await axios.post(`${this.gatewayHost}users/authenticate`, {
        //     "email": this.adminEmail,
        //     "password": this.adminPass,
        //     "remember": "true"
        // })
        try {
            let kpiResponse: any = []
            Instance.get().logger.info(`${this.gatewayHost}drivers`)
            const res = await axios.get(`${this.gatewayHost}drivers`,
                { headers: { Authorization: `Bearer ${this.token}` } })
            const driversResponse: any = res.data;

            if (driversResponse?.code === 8) {
                Instance.get().logger.info("got drivers")
                const drivers: any = driversResponse?.donne
                io.emit("drivers", driversResponse)
                let infos = [];
                let actions = [];
                let indicators = [];
                let meters = [];

                //loop on a multi max iteration
                for (let i = 0; i * maxSensor < drivers.length; i++) {
                    this.promises = []
                    for (let y = i * maxSensor; y < (i + 1) * maxSensor && y < drivers.length; y++) {
                        const driver = drivers[y]
                        infos = driver?.version?.infos?.map((e: any) => e.identifier);
                        actions = driver?.version?.actions?.map((e: any) => e.identifier);
                        meters = driver?.version?.meters?.map((e: any) => e.identifier);
                        indicators = driver?.version?.indicators?.map((e: any) => e.identifier);
                        Instance.get().logger.info(`scanning driver with IP : ${driver.IP}`)

                        this.promises.push(await this.scan(
                            driver.IP,
                            driver.port,
                            [{ name: "infos", values: infos },
                            { name: "actions", values: actions },
                            { name: "indicators", values: indicators },
                            { name: "meters", values: meters }],
                            driver.protocol,
                            driver.retries,
                            driver.timeout,
                            driver._id))
                    }

                    //mini iteration finished
                    this.promises?.map((e) => {
                        if (e.code == 22)
                            return kpiResponse.push(this.toKpiObject(e))
                    }
                    )

                    Instance.get().logger.info(`sending socket`)

                    io.emit("update", kpiResponse)

                }
            }
        } catch (e: any) {
            Instance.get().logger.error(`${e.message}, check if drivers service is running`)
        }
    }

    toKpiObject(ekpi: any) {
        try {
            let infos: any = [];
            let actions: any = [];
            let indicators: any = [];
            let meters: any = [];
            let driverId = ekpi.donne?.driverId
            ekpi.donne?.kpis.map((e: any) => {

                if (e.var == "infos")
                    infos.push({
                        identifier: e.oid,
                        value: e.value,
                        createdAt: e.date
                    })
                else
                    if (e.var == "meters") {
                        meters.push({
                            identifier: e.oid,
                            value: Math.random() * 100,
                            createdAt: e.date,
                        })
                    }
                    else
                        if (e.var == "indicators")
                            indicators.push({
                                identifier: e.oid,
                                value: e.value,
                                createdAt: e.date
                            })
                        else
                            if (e.var == "actions")
                                actions.push({
                                    identifier: e.oid,
                                    value: e.value,
                                    createdAt: e.date
                                })
            })
            const kpiHistoryObject = {
                severity1: Math.random() > 0.5 ? (Math.random() * 10).toFixed(0) : 0,
                severity2: Math.random() > 0.5 ? (Math.random() * 10).toFixed(0) : 0,
                severity3: Math.random() > 0.5 ? (Math.random() * 10).toFixed(0) : 0,
                actions, meters, infos, indicators, driverId
            }
            this.createKpiHistory(kpiHistoryObject)
            return kpiHistoryObject
        } catch (e: any) {
            Instance.get().logger.error(e)
        }
    }

    async set(ip = "localhost", port = 161, actions = [],
        protocol = "SNMP v2", retries = 0, timeout = 5000): Promise<HttpException> {

        protocol = protocol === "SNMP v2" ? snmp.Version2c : snmp.Version1

        const session = new snmp.createSession(ip, "public",
            { port, timeout, retries, version: protocol });
        const res: HttpException = await new Promise((resolve) =>
            session.set(actions, function (error: any, varbinds: any) {
                if (error) {
                    if (error.message === "Request timed out")
                        resolve(new WrongDevice400Response(ip, port.toString()))
                    else if (error.code === "ENOTFOUND")
                        resolve(new IPHostNotFound400Response(error.hostname, port.toString()))
                    else if (error.status === 2)
                        resolve(new NoDataReturnedFromOID400Response(`${error.message.split(":")[error.message.split(":").length - 1]}`))
                    else
                        resolve(new BadRequest400Response(error.message))
                } else {
                    for (const element of varbinds) {
                        // for version 1 we can assume all OIDs were successful
                        console.log(element.oid + "|" + element.value);

                        // for version 2c we must check each OID for an error condition
                        // if (snmp.isVarbindError(element))
                        //     console.error(snmp.varbindError(element));
                        // else
                        //     console.log(element.oid + "|" + element.value);
                    }
                }
                resolve(new SetActionSuccess200Response())
            }))

        await session.close();
        return res;
    }

    async scan(ip = "localhost", port = 161, variables = [{}],
        protocol = "SNMP v2", retries = 0, timeout = 5000, driverId = ""): Promise<HttpException> {

        protocol = protocol === "SNMP v2" ? snmp.Version2c : snmp.Version1

        const session = new snmp.createSession(ip, "public",
            { port, timeout, retries, version: protocol });

        let data: any = []

        var oids = variables?.map((e: any) => {
            return e?.values
        }).join().split(',').filter((e: any) => {
            if (e.length > 0) {
                return e[0] === '.' ? e.slice(1) : e
            }

        })
        const res: HttpException = await new Promise((resolve) =>
            session.get(oids, (error: any, varbinds: any) => {
                if (error) {
                    if (error.message === "Request timed out") {
                        resolve(new WrongDevice400Response(ip, port.toString()))
                    }
                    else if (error.code === "ENOTFOUND") {
                        resolve(new IPHostNotFound400Response(error.hostname, port.toString()))
                    }
                    else if (error.status === 2) {
                        resolve(new NoDataReturnedFromOID400Response(`${error.message.split(":")[error.message.split(":").length - 1]}`))
                    } else {
                        resolve(new BadRequest400Response(error.message))
                    }
                } else {
                    varbinds?.forEach((v: any) => {
                        let str = ""

                        for (let variable of variables) {

                            if ((variable as any)?.values?.includes(v.oid)) {
                                str = (variable as any)?.name
                                break;
                            }
                        }

                        data = [...data, { var: str, oid: `${v.oid}`, value: `${v.value}`, type: `(${v.type})`, date: moment().toISOString() }]

                    });
                    resolve(new ConnectedWithDevice200Response(data, driverId))
                }

            }))
        await session.close();

        return res

    }
    async getKpiHistory(skip = 0, limit = 0): Promise<HttpException> {
        const data = await this.kpiHistoryRepository.getKpiHistorys(skip, limit)
        return new Found200Response('KpiHistory', data.KpiHistorys.length ?? 0, data.KpiHistorys, data.pagination)
    }


    async createKpiHistory(data: { [key: string]: any }): Promise<HttpException> {

        const field: string = this.checkRequired(data)
        if (field == 'true') {
            const KpiHistory = await this.kpiHistoryRepository.createKpiHistory(data);
            return new Created201Response('KpiHistory', KpiHistory);
        }
        else return new FieldMissing400Response(field)
    }

    async updateKpiHistory(data: { [key: string]: any }): Promise<HttpException> {
        if (data._id) {
            if (isValidObjectId(data._id)) {
                const res: object = await this.kpiHistoryRepository.patchKpiHistory(data);
                if (res !== null)
                    return new Updated200Response('KpiHistory', data)
                else
                    return new NotFound404Response(data._id, 'KpiHistory');
            } else
                return new NotUUID404Response();
        } else
            return new FieldMissing400Response();
    }

    async deleteKpiHistory(id: string): Promise<HttpException> {
        if (id) {
            if (isValidObjectId(id)) {
                const res: object = await this.kpiHistoryRepository.deleteKpiHistory(id);
                if (res !== null)
                    return new Deleted200Response('KpiHistory', { _id: id })
                else
                    return new NotFound404Response(id, 'KpiHistory');
            } else
                return new NotUUID404Response();
        } else
            return new FieldMissing400Response()
    }


    async deleteKpiHistoryByDriverId(driverId: string): Promise<HttpException> {
        if (driverId) {
            if (isValidObjectId(driverId)) {
                const res: object = await this.kpiHistoryRepository.deleteKpiHistoryByDriverId(driverId);
                if (res !== null)
                    return new Deleted200Response('KpiHistory', { driverId: driverId })
                else {
                    return new NotFound404Response(driverId, 'KpiHistory');
                }
            } else
                return new NotUUID404Response();
        } else
            return new FieldMissing400Response()
    }


    public checkRequired(json: { [key: string]: any }): string {
        if (isNullEmptyOrWhiteSpace(json.driverId))
            return 'driverId'
        return 'true'
    }

    public checkDateRequired(json: { [key: string]: any }): string {
        if (isNullEmptyOrWhiteSpace(json.from))
            return 'from'
        else
            if (isNullEmptyOrWhiteSpace(json.to))
                return 'to'

        return 'true'
    }
}