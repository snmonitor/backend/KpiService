import { Schema } from "mongoose";

export const KpiSchema: Schema = new Schema(
    {
        name: String,
        value: String,
        identifier: String,
        resourceSha1: String,
        templateUuid: String,
        type: String

    }, { timestamps: true }
);