import {Schema} from "mongoose";
import {IAction} from "../interfaces";
import {KpiSchema, ValueSchema} from './index'

export const ActionSchema: Schema<IAction> = new Schema<IAction>(
    {
        ...KpiSchema.obj,
        description: String,
        valueType: String,
        values: [ValueSchema]
    }, {timestamps: true}
);