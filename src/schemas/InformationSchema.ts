import {Schema} from "mongoose";
import {IInformation} from "../interfaces"
import {KpiSchema} from "./index";

export const InformationSchema: Schema<IInformation> = new Schema<IInformation>(
    {
        ...KpiSchema.obj,
    }, {timestamps: true}
);