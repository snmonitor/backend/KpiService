import { Schema } from "mongoose";
import { IMeter } from "../interfaces"
import { KpiSchema } from "./index";

export const MeterSchema: Schema<IMeter> = new Schema<IMeter>(
    {
        ...KpiSchema.obj,
        unit: String,
        minValue: Number,
        maxValue: Number,
        precision: Number,
        mathExpression: String,
    }, { timestamps: true }
);