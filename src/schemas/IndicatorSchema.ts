import {Schema} from "mongoose";
import {IIndicator} from "../interfaces"
import {KpiSchema} from "./index";

export const IndicatorSchema: Schema<IIndicator> = new Schema<IIndicator>(
    {
        ...KpiSchema.obj,
        expectedValue: String,
        pushCorrelationKeys: [String],
        operator: String,
    }, {timestamps: true}
);