import { model, Schema } from "mongoose";
import { ActionSchema } from "./ActionSchema";
import { InformationSchema, IndicatorSchema, MeterSchema } from "./index";


export const KpiHistorySchema: Schema = new Schema(
     {
          driverId: { type: String, required: true },
          severity1: Number,
          severity2: Number,
          severity3: Number,
          actions: [ActionSchema],
          meters: [MeterSchema],
          infos: [InformationSchema],
          indicators: [IndicatorSchema],
     }, { timestamps: true }
);

export const KpiHistoryModel = model('KpiHistory', KpiHistorySchema, 'KpiHistory')