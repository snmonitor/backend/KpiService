import {Schema} from "mongoose";
import {IValue} from "../interfaces"

export const ValueSchema: Schema<IValue> = new Schema<IValue>(
    {
        value: String,
        templateUuid: String,
        name: String,

    }, {timestamps: true}
);