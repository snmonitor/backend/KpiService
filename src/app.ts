import * as express from "express";
import bodyParser from "body-parser";
import cors from 'cors'
import fs from 'fs-extra'
import morgan, { StreamOptions } from "morgan";
import Instance from './instance'
import { Controller } from "./Controller";
import swaggerUi from 'swagger-ui-express';
import swaggerDoc from './swagger';
import http from "http";
import { Server } from "socket.io"
import { KpiHistoryService } from "./services";
import moment from "moment";
export default class App {

    private _app: express.Application
    private kpiHistoryService = new KpiHistoryService();
    get app(): express.Application {
        return this._app
    }

    constructor(controllers: Controller[]) {
        this._app = express.default()

        if (process.env.NODE_ENV !== "test")
            this.initializingLogger();
        this.initializingMiddleware();
        this.initializingController(controllers);
        this._app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerDoc));
    }

    initializingMiddleware() {
        this._app.use(bodyParser.urlencoded({ extended: false, limit: '5mb' }));
        this._app.use(bodyParser.json({ limit: '5mb' }));
        this._app.use(cors());
    }

    initializingMonitoring(httpSocket: http.Server) {
        const io: Server = new Server(httpSocket, { upgradeTimeout: 10000, cors: { origin: "*" } })
        this.kpiHistoryService.monitor(io)
    }

    // initializeSwagger() {
    //     const swaggerSpec: Options = {
    //         definition: swaggerDoc,
    //         apis:["./src/controllers/*.ts","./src/schemas/*.ts"]
    //     }
    //     this._app.use('/api/docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec));
    // }

    initializingController(controllers: Controller[]) {
        controllers.forEach((e) => {
            this._app.use(`/api/${e.name}`, e.controller,);
        })
    }

    start(port: number, message: string) {

        const httpSocket = http.createServer(this.app)

        httpSocket.listen(port, () => {
            Instance.get().logger.info(message)
            Instance.get().logger.info(`env : ${process.env.NODE_ENV}`)
            Instance.get().logger.info('swagger available at /api/docs ')
            this.initializingMonitoring(httpSocket)
        })
        process.on('SIGINT', () => {
            Instance.get().logger.info('process exit')
            process.exit(0);
        });

    }

    dateGenerator() {

        return `${moment().toISOString().split("T")[0]}-${moment().toISOString().split("T")[1].split(':')[1]}.log`;
    }

    initializingLogger() {
        // checking if the folder log exist
        if (!fs.existsSync('./log')) {
            fs.mkdirSync('./log');
        }
        const stream: StreamOptions = {
            write: (message) =>
                Instance.get().logger.http(message),
        };
        this._app.use(morgan(':method/:status :url, :total-time[3]ms', { stream }))
    }
}



