import {HttpException} from "./index";

export class JWTExpired401Response extends HttpException {
    constructor() {
        super([],23, 401, `Jwt expired`);
    }
}

