import {HttpException} from "./index";

export class NotUpdated400Response extends HttpException {
    constructor(str: string = '', data: object = {}) {
        super([str],13, 400, `${str} not updated`, data);
    }
}