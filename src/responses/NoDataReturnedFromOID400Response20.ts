import {HttpException} from "./index";

export class NoDataReturnedFromOID400Response extends HttpException {
    constructor(OID=".0.0.0.0.0.0.") {
        super([OID],20, 400, `No data returned from the OID :${OID}`);
    }
}