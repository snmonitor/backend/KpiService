import {HttpException} from "./index";

export class IncorrectCredential401Response extends HttpException {
    constructor() {
        super([],9, 401, 'email or password are incorrect');
    }
}

