import {HttpException} from "./index";

export class PasswordTooShort400Response extends HttpException {
    constructor() {
        super([],15, 400, `The password is short it should contain 6 character mini`);
    }
}