import {HttpException} from "./index";

export class Deleted200Response extends HttpException {
    constructor(str: string = '', data: object) {
        super([str],4, 200, `${str} deleted`, data);
    }
}