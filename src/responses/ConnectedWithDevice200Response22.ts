import {HttpException} from "./index";

export class ConnectedWithDevice200Response extends HttpException {
    constructor(data = {},driverId:string) {
        super([],22, 200,"Connected with device", {driverId:driverId,kpis:data});
    }
}