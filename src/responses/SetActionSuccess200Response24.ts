import { HttpException } from "./index";

export class SetActionSuccess200Response extends HttpException {
     constructor() {
          super([], 24, 200, `The action is set with success`);
     }
}