import {HttpException} from "./index";

export class NotFound404Response extends HttpException {
    constructor(id: string = "", entity = 'user') {
        super([entity,id],12, 404, `${entity} with _id: ${id} not found`);
    }
}

