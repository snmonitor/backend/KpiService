import {HttpException} from "./index";

export class Unauthorized401Response extends HttpException {
    constructor(message='') {
        super([message],16, 401,`Unauthorized you must authenticate to get the requested response : ${message}`);
    }
}