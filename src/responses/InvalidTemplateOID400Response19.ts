import {HttpException} from "./index";

export class InvalidTemplateOID400Response extends HttpException {
    constructor() {
        super([],19, 400,"Invalid template OID format");
    }
}