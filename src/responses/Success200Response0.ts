import {HttpException} from "./index";

export class Success200Response extends HttpException {
    constructor(data = {}) {
        super([],0, 200,'success', data);
    }
}