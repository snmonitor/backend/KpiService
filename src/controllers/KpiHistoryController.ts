import { UserRole } from '../enums/UserRole';
import express, { Request, Response } from 'express'
import { Controller } from "../Controller";
import { KpiHistoryService } from "../services";
import { authorize } from '../middlewares/authMiddleware'
import { HttpException, InternalServerError500Response } from "../responses";

const kpiHistoryController = express.Router()
const kpiHistoryService = new KpiHistoryService()

kpiHistoryController.route('/')
    .get(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.getKpiHistory(parseInt(req.query.skip?.toString() ?? "0"),
                parseInt(req.query.limit?.toString() ?? "0"));
            response.send(res)

        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .post(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.createKpiHistory(req.body)
            response.send(res);

        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .put(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.updateKpiHistory(req.body);
            response.send(res);
        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    }).delete(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.deleteKpiHistory(req.body._id)
            response.send(res);
        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })



kpiHistoryController.route('/driverId/:id')
    .get(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.getKpiHistoryByDriver(req.params.id);
            response.send(res)

        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .delete(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.deleteKpiHistoryByDriverId(req.params.id)
            response.send(res);
        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

kpiHistoryController.route('/date')
    .post(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.getKpiHistoryByDate(req.body?.from,
                req.body?.to, req.body?.driverId);
            response.send(res)

        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

kpiHistoryController.route('/set')
    .post(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.set(req.body.IP, req.body.port,
                req.body.actions,
                req.body.protocol,
                req.body.retries,
                req.body.timeout);
            response.send(res)

        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

kpiHistoryController.route('/:id')
    .get(authorize(), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.getKpiHistoryById(req.params.id);
            response.send(res)

        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .put(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.updateKpiHistory({ _id: req.params.id, ...req.body })
            response.send(res);
        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })
    .delete(authorize([UserRole.admin]), async (req: Request, res: Response) => {
        try {
            const response: HttpException = await kpiHistoryService.deleteKpiHistory(req.params.id)
            response.send(res);
        } catch (e: any) {
            new InternalServerError500Response(e.message).send(res);
        }
    })

kpiHistoryController.route('*')
    .delete(async (_req: Request, res: Response) => {

        new InternalServerError500Response().send(res);
    })


export const KpiHistoryController = new Controller('kpiHistorys', kpiHistoryController);